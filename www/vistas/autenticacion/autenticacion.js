angular.module('simapag.controladores')
.controller('AutenticacionCtrl', function ($scope, ServicioPeticiones, $state, Sesion, ServicioLoading, ServicioAlertas, ServicioUtilidades) {
	$scope.usuario = {
		no_empleado: '2031',
		password: '3001'
	};

	$scope.autenticar = function() {
		ServicioUtilidades.ocultarTeclado();
		var resumen;

		ServicioLoading.mostrarLoading('Accesando');

		ServicioPeticiones.loguear($scope.usuario)
		.then(function(datos) {
			resumen = datos;

			return ServicioPeticiones.obtenerAnomalias();
		})
		.then(function(anomalias) {
			almacenarDatos('sesion', resumen);
			almacenarDatos('anomalias', anomalias);
			//$scope.usuario = {};
			Sesion.datos = resumen;
			ServicioLoading.ocultarLoading();
			$state.go('tomaLecturas');
		})
		.catch(ServicioAlertas.mostrarMensaje);
	};
});