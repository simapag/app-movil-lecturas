angular.module('simapag.servicios')

.service('ServicioSqlite', function ($cordovaSQLite, $q, ServicioAlertas, $ionicPlatform, FILAS_PAGINACION, FORMATO_FECHAS, FORMATO_HORAS) {
	var servicio = this;
	var db = null;

	servicio.testEcho = function() {
		var q = $q.defer();

		window.sqlitePlugin.echoTest(function() {
			console.log('Prueba Echo exitosa');
			q.resolve();
		}, function(error) {
			console.error('Prueba Echo fallida', error);
			q.reject();
		});

		return q.promise;
	};

	servicio.testSelf = function() {
		var q = $q.defer();

		window.sqlitePlugin.selfTest(function() {
			console.log('Prueba Test exitosa');
			q.resolve();
		}, function(error) {
			console.error('Prueba Test fallida', error);
			q.reject();
		});

		return q.promise;
	};

	servicio.verificarPluginBD = function() {
		return servicio.testEcho()
		.then(servicio.testSelf)
		.then(function() {
			console.log('Funcionamiento del engine de BD correcto');
		})
		.catch(function(error) {
			console.log('Funcionamiento del engine de BD incorrecto');
			return $q.reject();
		});
	};

	servicio.ejecutarQuery = function(query, datos) {
		//console.log('Query:', query);

		return $cordovaSQLite.execute(db, query, datos || [])
		.then(null, function(error) {
			console.error(error);
			return $q.reject('Ocurrio un error en el almacenamiento interno');
		});
	};

	servicio.crearBD = function() {
		db = $cordovaSQLite.openDB({ name: 'simapag.db', location: 'default' });

		var query =
			'CREATE TABLE IF NOT EXISTS predios (id integer primary key, rpu text, noCuenta text, nombreUsuario text, direccion text, noMedidor text, marcaMedidor text, diametroMedidor text, nombreTarifa text, ' +
			'abreviaturaTarifa text, lectura real, anomaliaUno text, anomaliaDos text, anomaliaTres text, comentarioUno text, comentarioDos text, comentarioTres text, comentarioCuatro text, uri text, latitud real, ' +
			'longitud real, estatus text, lecturaAnterior real, fecha text, hora text);';

		return this.ejecutarQuery(query)
		.then(function() {
			console.log('Base de datos abierta o creada correctamente');
		});
	};

	servicio.insertarPredios = function(predios) {
		var query = 'insert into predios (rpu, noCuenta, nombreUsuario, direccion, noMedidor, marcaMedidor, diametroMedidor, nombreTarifa, abreviaturaTarifa, lecturaAnterior) values';

		for(var i = 0; i < predios.length; i++){
			var predio = predios[i];

		    query += '("' + predio.Rpu +'", "' + predio.No_Cuenta + '", "' + predio.Usuario + '",	"' + predio.Direccion +'", 	"' + predio.Serie_Medidor + '", "' +
		    	predio.Marca + '", "' + predio.Diametro + '", "' + predio.Tarifa  + '", "' + predio.Tarifa + '",' + predio.Lectura_Actual + ')';

		    if(i + 1 < predios.length)
		        query += ',';
		}

		return servicio.ejecutarQuery(query).then(null, function(error) {
			console.error(error);
			return $q.reject('Ocurrio un error en el almacenamiento interno');
		});
	};

	servicio.obtenerTotalCuentas = function() {
		return this.ejecutarQuery('select count(*) as totalCuentas from predios')
		.then(function(restultado) {
			return restultado.rows.item(0).totalCuentas;
		});
	};

	servicio.paginar = function(paginaActual) {
		var inicio, query;
		inicio = (paginaActual * FILAS_PAGINACION) - FILAS_PAGINACION;
		query = 'select * from predios limit ? offset ?';

		return servicio.ejecutarQuery(query, [FILAS_PAGINACION, inicio])
		.then(function(resultado) {
			var predios = [];

			for(var i = 0; i < resultado.rows.length; i++){

				var cuenta = resultado.rows.item(i);
				cuenta.anomalias = [];
				cuenta.comentarios = [];

				if(cuenta.anomaliaUno)
					cuenta.anomalias.push(cuenta.anomaliaUno);

				if(cuenta.anomaliaDos)
					cuenta.anomalias.push(cuenta.anomaliaDos);

				if(cuenta.anomaliaTres)
					cuenta.anomalias.push(cuenta.anomaliaTres);

				if(cuenta.comentarioUno)
					cuenta.comentarios.push(cuenta.comentarioUno);

				if(cuenta.comentarioDos)
					cuenta.comentarios.push(cuenta.comentarioDos);

				if(cuenta.comentarioTres)
					cuenta.comentarios.push(cuenta.comentarioTres);

				if(cuenta.comentarioCuatro)
					cuenta.comentarios.push(cuenta.comentarioCuatro);

				delete cuenta.anomaliaUno;
				delete cuenta.anomaliaDos;
				delete cuenta.anomaliaTres;
				delete cuenta.comentarioUno;
				delete cuenta.comentarioDos;
				delete cuenta.comentarioTres;
				delete cuenta.comentarioCuatro;

				predios.push(cuenta);
			}
			
			console.log('Predios:', predios);
			return predios;			
		});
	};

	servicio.actualizarCuenta = function(cuenta) {
		var query = 'UPDATE predios SET ';

		cuenta.fecha = moment().format(FORMATO_FECHAS);
		cuenta.hora = moment().format(FORMATO_HORAS);		
		
		if(!cuenta.lectura)
			cuenta.lectura = 0;

		//query +='lectura=' + (cuenta.lectura ? cuenta.lectura : 0);
		query +='lectura=' + cuenta.lectura;

		query += ', anomaliaUno=' + (cuenta.anomalias.length > 0 ? ('"' + cuenta.anomalias[0] + '"') : 'null');
		query += ', anomaliaDos=' + (cuenta.anomalias.length > 1 ? ('"' + cuenta.anomalias[1] + '"') : 'null');
		query += ', anomaliaTres=' + (cuenta.anomalias.length > 2 ? ('"' + cuenta.anomalias[2] + '"') : 'null');

		query += ', comentarioUno=' + (cuenta.comentarios.length > 0 ? ('"' + cuenta.comentarios[0] + '"') : 'null');
		query += ', comentarioDos=' + (cuenta.comentarios.length > 1 ? ('"' + cuenta.comentarios[1] + '"') : 'null');
		query += ', comentarioTres=' + (cuenta.comentarios.length > 2 ? ('"' + cuenta.comentarios[2] + '"') : 'null');
		query += ', comentarioCuatro=' + (cuenta.comentarios.length > 3 ? ('"' + cuenta.comentarios[3] + '"') : 'null');

		query +=',uri=' + (cuenta.uri ? ('"' + cuenta.uri + '"' ) : 'null');
		
		query +=',latitud=' + (cuenta.latitud ? cuenta.latitud : 'null');
		query +=',longitud=' + (cuenta.longitud ? cuenta.longitud : 'null');
		
		query +=',fecha="' + cuenta.fecha + '"';
		query +=',hora="' + cuenta.hora + '"';



		query +=',estatus= "capturado" where id=' + cuenta.id;

		console.log(query);

		return servicio.ejecutarQuery(query)
			.then(null, function(error) {
				console.error('Error al modificar la lectura', error);
				return $q.reject('Error de alamacenamiento interno');
			});
	};

	servicio.limpiarBD = function() {
		return servicio.ejecutarQuery('delete from predios')
		.then(function() {
			console.log('Base datos limpiada correctamente');
		}, function(error) {
			console.error(error);
			return $q.reject('Error al limpiar la base de datos');
		});
	};

	servicio.consultarPredios = function() {
		return this.ejecutarQuery('select * from predios')
		.then(function(result){
			var predios = [];

			for(var i = 0; i < result.rows.length; i++)
				predios.push(result.rows.item(i));
				
			return predios;
		});
	};

	servicio.eliminarCuenta = function(cuenta) {
		var query = 'delete from predios where id = ?';

		return servicio.ejecutarQuery(query, [cuenta.id]).then(null, function(error) {
			console.error(error);
			return $q.reject('Ocurrio un error al consultar las cuentas');
		});
	};

	servicio.insertarPredio = function(predio) {
		var datos = [predio.Rpu, predio.No_Cuenta, predio.Usuario, predio.Direccion, predio.noMedidor, predio.Marca, predio.Diametro, predio.Tarifa, predio.Tarifa];
		var query = 'insert into predios (rpu, noCuenta, nombreUsuario, direccion, noMedidor, marcaMedidor, diametroMedidor, nombreTarifa, abreviaturaTarifa) values (?, ?, ?, ?, ?, ?, ?, ?, ?)';

		return servicio.ejecutarQuery(query, datos).then(null, function(error) {
			console.log('No se inserto');
			return $q.reject();
		});
	};

	servicio.buscarCuenta = function(filtro) {
		var query = 'select id from predios where cast(rpu as int) = ? or cast(noCuenta as int) = ? or cast(noMedidor as int) = ?';
		return servicio.ejecutarQuery(query, [filtro, filtro, filtro])
		.then(function(resultado) {
			if(resultado.rows.length < 1)
				return $q.reject('Cuenta no encontrada');

			return resultado.rows.item(0).id;
		});
	};

	servicio.obtenerCuentasConImagenes = function() {
		return this.ejecutarQuery('SELECT id, noCuenta, uri FROM predios where uri is not null')
		.then(function(result){
			var predios = [];

			for(var i = 0; i < result.rows.length; i++)
				predios.push(result.rows.item(i));
				
			return predios;
		});
	};

	servicio.eliminarUri = function(cuenta) {
		var query = 'update predios set uri = null where id = ?';

		return servicio.ejecutarQuery(query, [cuenta.id]).then(null, function(error) {
			console.error(error);
			return $q.reject('Ocurrio un error al eliminar la uri las cuentas');
		});
	};
});