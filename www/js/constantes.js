angular.module('simapag.constantes', [])

.constant('URL', 'http://200.33.34.70/SIMAPAG_AppLecturas/siac/')
// .constant('URL', 'http://172.16.0.97/SIMAPAG_AppLecturas/siac/')
// .constant('URL', 'http://172.16.1.43:3000/SIMAPAG_AppLecturas/siac/')
// .constant('URL', 'http://172.16.1.96:3000/')
.constant('FILAS_PAGINACION', 25)
.constant('TIEMPO_ESPERA_GPS', 1000 * 5)
.constant('DURACION_ALERTAS', 1000 * 5)
.constant('TIEMPO_ESPERA_PETICIONES', 1000 * 15)
.constant('TIEMPO_ESPERA_PETICIONES_PESADAS', 1000 * 60 * 2)
.constant('FORMATO_FECHAS', 'DD/MM/YYYY')
.constant('FORMATO_HORAS', 'HH:mm:SS')
.constant('TIEMPO_ESPERA_PETICIONES_PESADAS', 1000 * 60 * 2);