angular.module('simapag.servicios')

.service('ServicioGPS', function ($cordovaGeolocation, TIEMPO_ESPERA_GPS, $q, ServicioAlertas) {
	var servicio = this;
	var opciones = { maximumAge: 3000, timeout: TIEMPO_ESPERA_GPS, enableHighAccuracy: true };

	servicio.obtenerUbicacion = function() {
		return $cordovaGeolocation.getCurrentPosition(opciones)
		.then(function(ubicacion) {
			console.log('Ubiación obtenida', ubicacion);

			return {
				latitud: ubicacion.coords.latitude,
				longitud: ubicacion.coords.longitude	
			};
		}, function(error) {
			console.error('No se pudo obtener la ubicación', error);

			return {
				latitud: null,
				longitud: null	
			};
		});
	};
});