angular.module('simapag.controladores', []);
angular.module('simapag.servicios', ['ngCordova', 'simapag.constantes']);
angular.module('simapag', ['ionic', 'simapag.controladores', 'simapag.servicios', 'simapag.valores', 'simapag.factories'])

.config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
    $stateProvider

    .state('autenticacion', {
        url: '/autenticacion',
        templateUrl: 'vistas/autenticacion/autenticacion.html',
        controller: 'AutenticacionCtrl'
    })

    .state('tomaLecturas', {
        url: '/tomaLecturas',
        templateUrl: 'vistas/tomaLecturas/tomaLecturas.html',
        controller: 'TomaLecturasCtrl',
        cache: false
    });

    if(obtenerAlamacenamiento('iniciada'))
        $urlRouterProvider.otherwise('/tomaLecturas');
    else
        $urlRouterProvider.otherwise('/autenticacion');

    $httpProvider.interceptors.push('httpInterceptor');
})

.run(function($ionicPlatform, Sesion, ServicioSqlite, ServicioDispositivo, httpInterceptor, ExpresionesRegulares, $rootScope) {

    $ionicPlatform.registerBackButtonAction(function() {
        ionic.Platform.exitApp();
    }, 100);

    if(obtenerAlamacenamiento('iniciada'))
        Sesion.datos = obtenerAlamacenamiento('sesion');
    
    $ionicPlatform.ready(function() {
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);
        }

        if (window.StatusBar)
            StatusBar.styleDefault();

        ServicioSqlite.crearBD()
        .then(ServicioDispositivo.iniciar);
    });

    $rootScope.regexs = ExpresionesRegulares;
});
