angular.module('simapag.factories', ['simapag.constantes'])

.factory('httpInterceptor', function ($q, URL, TIEMPO_ESPERA_PETICIONES, $injector) {
	var expresionRegular = new RegExp('^' + URL);
	var tiempoInicio;

	return {
	    request: function(configuracion) {
	    	if(expresionRegular.test(configuracion.url)){
	    		if(!configuracion.timeout)
	    			configuracion.timeout = TIEMPO_ESPERA_PETICIONES;
	    		tiempoInicio = new Date().getTime();
	    	}

	        return configuracion;
	    },

	    requestError: function(rechazo) {  return $q.reject(rechazo); },

	    response: function(respuesta) {
	    	if(expresionRegular.test(respuesta.config.url))
		    	console.log(respuesta.data);
	    	return respuesta;
	    },

	    responseError: function(rechazo) {
	    	if(expresionRegular.test(rechazo.config.url)){
	    		var tiempoRespuesta = new Date().getTime() - tiempoInicio;
	    		var mensaje = Math.round(tiempoRespuesta / 1000) >= (rechazo.config.timeout / 1000) ? 'Tiempo de espera de conexión agotado' : 'Ha ocurrido un error de comunicaciones';
	    		rechazo.mensaje = mensaje;
	    	}

	        return $q.reject(rechazo);
	    }
	};
});