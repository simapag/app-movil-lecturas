angular.module('simapag.servicios').

service('ServicioDispositivo', function ($cordovaDevice) {
	var servicio = this;

	servicio.iniciar = function() {
		servicio.device = $cordovaDevice.getDevice();
		servicio.cordova = $cordovaDevice.getCordova();
		servicio.model = $cordovaDevice.getModel();
		servicio.platform = $cordovaDevice.getPlatform();
		servicio.uuid = $cordovaDevice.getUUID();
		servicio.version = $cordovaDevice.getVersion();	
	};
});