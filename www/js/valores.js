angular.module('simapag.valores', [])

.value('Sesion', {})

.value('ExpresionesRegulares', {
	numeros: '\\d+',
	numerosCantidades: '\\d{1,3}',
	numerosMaximo: '\\d{1,10}',
	letras: /^[a-z]+$/i,
	letrasEspacios: /^[a-z ]+$/i,
	numerosLetras: /^[a-z0-9]+$/i,
	numerosLetrasEspacios: /^[a-z0-9 ]+$/i,
	textosLargos: /^[a-z0-9ñ.,:;/()áéíóú ]{1,250}$/i,
	materiales: /^[a-z0-9ñ/()áéíóú° ]{1,250}$/i,
	ordenesTrabajo : '\\d{10}',
	rpu : '\\d{12}'
});