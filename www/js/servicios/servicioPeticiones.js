angular.module('simapag.servicios')

.service('ServicioPeticiones', function ($http, URL, ServicioAlertas, $q, ServicioDispositivo, ServicioSqlite, Sesion, $cordovaFileTransfer, ServicioLoading, TIEMPO_ESPERA_PETICIONES_PESADAS) {
	var servicio = this;

	servicio.loguear = function(usuario) {
		usuario.uuid = ServicioDispositivo.uuid;

		return $http.post(URL + 'Login', usuario)
		.then(function(datos) {
			var respuesta = datos.data;

			if(respuesta.Nombre_Empleado === 'Acceso Denegado')
				return $q.reject('Acceso Denegado');
			//else if (respuesta.estatus === 'TERMINADA' || respuesta.estatus === 'CANCELADA')
			else if (!respuesta.estatus || respuesta.estatus === 'TERMINADA' || respuesta.estatus === 'CANCELADA')
				return $q.reject('No se encontraron cuentas asignadas a este dispositivo');

			return respuesta;
		}, function(error) {
			console.error(error);
			return $q.reject(error.mensaje);
		});
	};

	servicio.obtenerAnomalias = function() {
		return $http.get(URL + 'Anomalias')
		.then(function(datos) {
			var respuesta = datos.data;

			if(respuesta.Anomalias instanceof Array)
				return respuesta.Anomalias;
			else
				return $q.reject('Ha ocurrido un error en el servidor');
		}, function(error) {
			console.error(error);
			return $q.reject(error.mensaje);
		});
	};

	servicio.obtenerCuentas = function() {
		var cadena = 'Carga_Cuentas/' + ServicioDispositivo.uuid;

		return $http.get(URL + cadena, {timeout: TIEMPO_ESPERA_PETICIONES_PESADAS})
		.then(function(datos) {
			var respuesta = datos.data;

			if(respuesta.Cuentas instanceof Array){

				if(respuesta.Cuentas.length < 1)
					return $q.reject('No se obtuvieron cuentas');	
				else
					return respuesta.Cuentas;
			}
			else
				return $q.reject('Ha ocurrido un error en el servidor');

		}, function(error) {
			console.error(error);
			return $q.reject(error.mensaje);
		});
	};

	servicio.empezarAsignacion = function() {
		return $http.get(URL + 'Iniciar_Ruta/' + Sesion.datos.Empleado_Id + '/' + Sesion.datos.no_asignacion)
		.then(function(datos) {
			var respuesta = datos.data;

			if(respuesta.Result !== 'Ok')
				return $q.reject('Ha ocurrido un error en el servidor');
		});
	};

	servicio.cargarCuentas = function() {
		return ServicioSqlite.consultarPredios()
		.then(mandarLecturas);
	};

	function mandarLecturas(cuentas) {
		parametro = {
			Lista_Lecturas : []
		};

		for (var i = 0; i < cuentas.length; i++) {
			var cuenta = cuentas[i];
			
			parametro.Lista_Lecturas.push({
				no_asignacion: Sesion.datos.no_asignacion,
				no_cuenta: cuenta.noCuenta,
				usuario: Sesion.datos.Nombre_Empleado,
				anomalia_1: cuenta.anomaliaUno,
				anomalia_2: cuenta.anomaliaDos,
				anomalia_3: cuenta.anomaliaTres,
				lectura: cuenta.lectura || 0,
				comentario_1: cuenta.comentarioUno,
				comentario_2: cuenta.comentarioDos,
				comentario_3: cuenta.comentarioTres,
				comentario_4: cuenta.comentarioCuatro,
				latitud: cuenta.latitud,
				longitud: cuenta.longitud,
				Fecha_Lectura: cuenta.fecha,
				Hora_Lectura: cuenta.hora
			});
		}

		return $http.post(URL + 'Exportar_Lecturas', parametro, {timeout: TIEMPO_ESPERA_PETICIONES_PESADAS})
		.then(function(datos) {
			var respuesta = datos.data;

			if(respuesta.Result === 'Ok')
				return mandarImagenes();//ServicioSqlite.limpiarBD();
			else
				return $q.reject(respuesta.Result || 'Ha ocurrido un error en el servidor');

		}, function(error) {
			console.error(error);
			return $q.reject(error.mensaje);
		});
	}

	function mandarImagenes() {
		ServicioLoading.cambiarLeyenda('Cargando imagenes');

		return ServicioSqlite.obtenerCuentasConImagenes()
		.then(function(cuentas) {
			console.log('Cuentas con uris', cuentas);

			var promesa = $q.when();

			angular.forEach(cuentas, function(cuenta) {
				promesa = promesa.then(function() {
					return servicio.cargarImagen(cuenta);
				});
			});

			return promesa;
		});
	}

	servicio.cargarImagen = function(cuenta) {
		return $cordovaFileTransfer.upload(URL + 'fileupload/stream/' + Sesion.datos.no_asignacion + '/' + cuenta.noCuenta, cuenta.uri, {timeout: TIEMPO_ESPERA_PETICIONES_PESADAS})
		  .then(function(res) {
		  	console.log('Imagen cargada correctamente');
		  	return ServicioSqlite.eliminarUri(cuenta);
		  }, function(error) {
			console.error(error);
			var mensaje;

			switch(error.code){
				case 4:
					mensaje = 'Tiempo de espera de conexión agotado';
				break;
				default:
					mensaje = 'Ocurrio un error al guardar la imagen';
				break;
			}

			return $q.reject(mensaje);
		}, function(progress) {
			//console.log(progress.loaded + ' de ' + progress.total);
		});
	};
});