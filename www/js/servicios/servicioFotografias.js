angular.module('simapag.servicios')

.service('ServicioFotografias', function ($cordovaCamera, $q) {
	var servicio = this;
	var opciones = {
		destinationType: null,
		sourceType: null,
		allowEdit: false ,
		saveToPhotoAlbum: false,
		quality: 100
	};

	servicio.tomarFoto = function(modo) {
		opciones.destinationType = Camera.DestinationType.FILE_URI;
		opciones.sourceType = Camera.PictureSourceType.CAMERA;

		if(modo)
			opciones.sourceType = Camera.PictureSourceType.PHOTOLIBRARY;

		return $cordovaCamera.getPicture(opciones)
		.then(function(uri) {
			console.log('Direccion de la foto:',uri);
			return uri;
		}, function(error) {
			console.error(error);
			return $q.reject();
		});
	};
});