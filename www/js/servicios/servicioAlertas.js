angular.module('simapag.servicios')

.service('ServicioAlertas', function ($ionicLoading, DURACION_ALERTAS, $ionicPopup) {
	var servicio = this;

	servicio.mostrarMensaje = function (msj) {
		return $ionicLoading.show({
			template: msj + '.',
			duration: DURACION_ALERTAS
		}); 
	};

	servicio.mostrarAlerta = function(mensaje, titulo) {
		// return $ionicPopup.alert({
		// 	title: titulo || 'Información',
		// 	template: '<center>' + mensaje + '.</center>'
		// });
		return $ionicPopup.show({
			title: titulo || 'Información',
			template: '<center>' + mensaje + '.</center>',
			buttons: [
				{
					text: '<b>Aceptar</b>',
					type: 'button-assertive',
				}
			]
		});
	};

	servicio.mostrarAlertaError = function(mensaje, titulo, boton) {
		return $ionicPopup.show({
			title: titulo || 'Información',
			template: '<center>' + mensaje + '.</center>',
			buttons: [
				{
					text: '<b>Reintentar</b>',
					type: 'button-assertive',
				}
			]
		});
	};
});