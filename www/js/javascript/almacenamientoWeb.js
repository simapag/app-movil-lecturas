function obtenerAlamacenamiento(key) {
	var datos = localStorage.getItem(key);
	return datos ? JSON.parse(datos) : null;
}

function almacenarDatos(key, datos) {
	localStorage.setItem(key, JSON.stringify(datos));
}