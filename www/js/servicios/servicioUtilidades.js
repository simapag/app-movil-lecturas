angular.module('simapag.servicios')

.service('ServicioUtilidades', function () {
	var servicio = this;
	
	servicio.ocultarTeclado = function () {
		if(window.cordova && window.cordova.plugins.Keyboard)
			cordova.plugins.Keyboard.close();
	};
});