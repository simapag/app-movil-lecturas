angular.module('simapag.servicios')

.service('ServicioLoading', function ($ionicLoading, $rootScope) {
	var servicio = this;
	var template = "<ion-spinner icon='crescent' class='spinner-light'></ion-spinner><br/><span>{{(leyendaLoading || 'Cargando') + '...'}}</span>";

	servicio.cambiarLeyenda = function(nuevaLeyenda) { $rootScope.leyendaLoading = nuevaLeyenda; };

	servicio.mostrarLoading = function(leyenda) {
		if(leyenda)
			this.cambiarLeyenda(leyenda);

		$ionicLoading.show({
		    template: template
		});		
	};

	servicio.ocultarLoading = function () {
	    $rootScope.leyendaLoading = null;
	    $ionicLoading.hide();
	};
});