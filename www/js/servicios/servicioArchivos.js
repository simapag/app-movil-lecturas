angular.module('simapag.servicios')

.service('ServicioArchivos', function ($cordovaFile, $q) {
	var servicio = this;

	function obtenerArchivos(path) {
	    var deferred = $q.defer();

	    window.resolveLocalFileSystemURL(path, function(fileSystem) {
	        var directoryReader = fileSystem.createReader();

	        directoryReader.readEntries(function(entries) {
	            deferred.resolve(entries);
	        }, function(error) {
	            deferred.reject(error);
	        });
	    }, function(error) {
	        deferred.reject(error);
	    });

	    return deferred.promise;
	}

	function obtenerArchivosDeCache() {
		return obtenerArchivos(cordova.file.externalCacheDirectory);
	}

	function eliminarFoto(fileEntry) {
		console.log('Foto a eliminar', fileEntry);

		$cordovaFile.removeFile(cordova.file.externalCacheDirectory, fileEntry.name)
		.then(function () {
			console.log('Foto ' + fileEntry.name + ' eliminada');
		});
	}

	function eliminarFotos(fileEntries) {
		console.log('Fotos a eliminar', fileEntries);

		var promesa = $q.when();

		angular.forEach(fileEntries, function(fileEntry) {
			promesa = promesa.then(function() {
				return eliminarFoto(fileEntry);
			});
		});

		return promesa;
	}

	servicio.eliminarFotosCache = function() {
		return obtenerArchivosDeCache()
		.then(eliminarFotos)
		.catch(function(error) {
			console.error(error);
			return $q.reject('Ocurrio un error al eliminar las imagenes');
		});
	};
});