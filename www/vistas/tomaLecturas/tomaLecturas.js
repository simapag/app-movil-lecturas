angular.module('simapag.controladores')
.controller('TomaLecturasCtrl', function ($scope, $ionicModal, ServicioSqlite, $ionicPlatform, ServicioAlertas, ServicioLoading, $ionicSlideBoxDelegate,
	FILAS_PAGINACION, $ionicActionSheet, $state, ServicioFotografias, Sesion, $q, ServicioPeticiones, ServicioGPS, $ionicPopup, $timeout, ServicioArchivos) {

	var totalElementos, paginaActual, index, deslisando;

	$scope.estado = {
		resumen: Sesion.datos,
		opcionesSlides: {
			loop: false,
			speed: 500,
			pagination: false
		},
		anomalias: [],
		totalPaginas: 0,
		paginaActual: 1,
		predios: [],
		cuentaActual: null,
		lectura: null,
		filtro: null
	};

	$scope.infoModalComentarios = {
		comentarioActual: null,
		comentarios: []
	};

	$scope.estado.anomalias = obtenerAlamacenamiento('anomalias') || [];

	$scope.$on("$ionicSlides.sliderInitialized", function(event, data){ $scope.slider = data.slider; });
	$scope.$on('$destroy', function() {
		$scope.modalPaginas.remove();
		$scope.modalAnomalias.remove();
	});
	$ionicPlatform.ready(refrescar);

	function refrescar() {
		ServicioLoading.mostrarLoading('Cargando');

		ServicioSqlite.obtenerTotalCuentas()
		.then(function(total) {
			totalElementos = total;

			var totalPaginas = totalElementos / FILAS_PAGINACION;
			
			if(totalPaginas % FILAS_PAGINACION !== 0)
				totalPaginas++;

			$scope.estado.totalPaginas = totalPaginas;

			if(totalElementos === 0)
				return $q.reject();

			var ultimo = 1;

			if(obtenerAlamacenamiento('progreso'))
				ultimo = parseInt(obtenerAlamacenamiento('progreso'));

			return posicionarEnSlideBox(ultimo);
			//return ServicioSqlite.paginar($scope.estado.paginaActual);
		})
		.then(function(predios) {
			$scope.estado.predios = predios;
			$scope.estado.cuentaActual = predios[index];
			$scope.slider.updateLoop();

			return $ionicModal.fromTemplateUrl('vistas/tomaLecturas/modalAnomalias.html', {
			    scope: $scope,
			    animation: 'slide-in-up'
			});
		})
		.then(function(modal) {
			$scope.modalAnomalias = modal;
				return $ionicModal.fromTemplateUrl('vistas/tomaLecturas/modalComentarios.html', {
				    scope: $scope,
				    animation: 'slide-in-up'
				});
		})
		.then(function(modal) {
			$scope.modalComentarios = modal;

			var modalPaginas = '<ion-modal-view><ion-header-bar class="bar-dark"><button class="button button-icon icon ion-ios-arrow-back" ng-click="ocultarModal()"></button><h1 class="title">Páginas</h1></ion-header-bar> <ion-content><ion-list>';

			for(var i = 1; i <= $scope.estado.totalPaginas; i++)
				modalPaginas += '<ion-radio ng-model="estado.paginaActual" ng-value=' + i +' ng-click="cambiarPagina(' + i +')">Pagina ' + i + '&nbsp;&nbsp;&nbsp;&nbsp;Del ' + 
				(((i * FILAS_PAGINACION) - FILAS_PAGINACION) + 1) + ' al ' + ( i + 1 > $scope.estado.totalPaginas ? totalElementos : (i * FILAS_PAGINACION) ) + '</ion-radio>';

			modalPaginas += '</ion-list></ion-content></ion-modal-view>';

			$scope.modalPaginas = $ionicModal
			.fromTemplate(modalPaginas, {
			    scope: $scope,
			    animation: 'slide-in-up'
			});
		})
		.catch(function(error) {
			if(!error) return;

			console.error(error);
			ServicioAlertas.mostrarAlerta('Ha ocurrido un error');
		})
		.finally(function() {

			if($scope.slider)
				$timeout(function() {
					deslisando = index === 0 ? false : true;
					$scope.slider.slideTo(index);
					ServicioLoading.ocultarLoading();
				}, 1000);
			else
				ServicioLoading.ocultarLoading();
		});
	}

	$scope.mostrarOpciones = function() {
		var botones;

		if(totalElementos === 0)
			botones = [
				{ text: 'Descargar datos' }
			];
		else{
			var mostrandoResumen = $scope.estado.mostrarResumen;

			if(mostrandoResumen)
				botones = [
					{ text: 'Ocultar resumen' }
				];
			
			else
				botones = [
					{ text: 'Cambiar de pagina' },
					{ text: 'Cargar datos' },
					{ text: 'Mostrar resumen' },
					{ text: 'Buscar' }
				];
		} 

		$ionicActionSheet.show({
		    buttons: botones,
		    buttonClicked: function(index) {
		    	switch (index) {
		    		case 0:
		    			if(totalElementos === 0)
			    			descargarCuentas();
			    		else{
			    			if(mostrandoResumen)
			    				$scope.estado.mostrarResumen = !$scope.estado.mostrarResumen;
			    			else
		    					mostrarPaginacion();
			    		}
	    			break;
	    			case 1:
	    				cargarCuentas();
	    			break;
	    			case 2:
	    				$scope.estado.mostrarResumen = !$scope.estado.mostrarResumen;
	    			break;
	    			case 3:
	    				buscar();
	    			break;
		    	}
		        return true;
		    }
		});
	};

	function descargarCuentas() {
		ServicioLoading.mostrarLoading('Consultando información');

		ServicioPeticiones.obtenerCuentas()
		.then(ServicioSqlite.insertarPredios)
		.then(ServicioPeticiones.empezarAsignacion)
		.then(function() {
			almacenarDatos('iniciada', true);
			ServicioLoading.ocultarLoading();
			ServicioAlertas.mostrarAlerta('Descarga exitosa', 'Información')
			.then(refrescar);
		})
		.catch(function(rechazo) {
			ServicioAlertas.mostrarMensaje(rechazo);
		});
	}

	function mostrarPaginacion() {
		paginaActual = $scope.estado.paginaActual;
		$scope.modalPaginas.show();
	}

	$scope.cambiarPagina = function(pagina) {
		if(paginaActual == $scope.estado.paginaActual)
			return;

		ServicioLoading.mostrarLoading('Actualizando');
		paginaActual = $scope.estado.paginaActual;

		ServicioSqlite.paginar($scope.estado.paginaActual)
		.then(function(predios) {
			$scope.estado.predios = predios;
			$scope.estado.cuentaActual = predios[0];
			$scope.slider.updateLoop();
			deslisando = index === 0 ? false : true;
			$scope.slider.slideTo(0);
			ServicioLoading.ocultarLoading();
			$scope.ocultarModal();
		});
	};

	function cargarCuentas() {
		ServicioLoading.mostrarLoading('Sincronizando cuentas');

		ServicioPeticiones.cargarCuentas()
		.then(ServicioPeticiones.mandarImagenes)
		.then(function(respuesta) {
			ServicioLoading.ocultarLoading();
			return ServicioAlertas.mostrarAlerta('Datos cargados exitosamente')
			.then(cerrarSesion);
		})
		.then(function() {
			$state.go('autenticacion');
		})
		.catch(function(error) {
			ServicioAlertas.mostrarAlertaError(error, 'Error').
			then(cargarCuentas);
		})
		.finally(function() {
			ServicioLoading.ocultarLoading();
		});
	}

	function cerrarSesion() {
		ServicioLoading.cambiarLeyenda('Cerrando sesión');

		almacenarDatos('sesion', '');
		almacenarDatos('anomalias', '');
		almacenarDatos('progreso', '');
		almacenarDatos('iniciada', '');

		return ServicioSqlite.limpiarBD()
		.then(ServicioArchivos.eliminarFotosCache);
	}

	$scope.$on("$ionicSlides.slideChangeStart", function(event, data){
		if(!deslisando)
			$scope.$apply(function() {
				var indexActual = data.slider.activeIndex;
				$scope.estado.cuentaActual = $scope.estado.predios[indexActual];
				console.log($scope.estado.cuentaActual);
			});
	});


	$scope.$on("$ionicSlides.slideChangeEnd", function(event, data){
		deslisando = false;
	});

	$scope.mostrarAnomalias = function() {
		var anomalias = $scope.estado.anomalias;
		var anomaliasCuentaActual = $scope.estado.cuentaActual.anomalias;

		if(anomaliasCuentaActual.length > 0)
			for (var i = 0; i < anomaliasCuentaActual.length; i++)
				for (var j = 0; j < anomalias.length; j++)
					if(anomaliasCuentaActual[i] === anomalias[j].clave){
						anomalias[j].estaSeleccionado = true;
						break;
					}

		$scope.modalAnomalias.show();
	};

	$scope.ocultarModalAnomalias = function() {
		var anomalias = $scope.estado.anomalias;
		var anomaliasCuentaActual = $scope.estado.cuentaActual.anomalias;

		anomaliasCuentaActual.length = 0;

		for (var i = 0; i < anomalias.length; i++)
			if(anomalias[i].estaSeleccionado)
				anomaliasCuentaActual.push(anomalias[i].clave);

		if(anomaliasCuentaActual.length > 3)
			ServicioAlertas.mostrarMensaje('Solo se permiten 3 anomalias');
		else{
			for (var ii = 0; ii < anomalias.length; ii++)
				anomalias[ii].estaSeleccionado = false;

			$scope.modalAnomalias.hide();	
		}
	};

	$scope.mostrarComentarios = function() {
		var comentarios = $scope.infoModalComentarios.comentarios;
		var comentariosCuentaActual = $scope.estado.cuentaActual.comentarios;

		if(comentariosCuentaActual.length > 0)
			for (var i = 0; i < comentariosCuentaActual.length; i++)
				comentarios.push({no: i + 1, texto: comentariosCuentaActual[i]});

		$scope.modalComentarios.show();
	};

	$scope.agregarComentario = function() {
		var infoModal = $scope.infoModalComentarios;
		var totalComentarios = infoModal.comentarios.length;

		//if(infoModal.comentarioActual && !infoModal.comentarioActual.no){
		if(!infoModal.comentarioActual.no){

			if(infoModal.comentarios.length > 3){
				ServicioAlertas.mostrarMensaje('Solo se permiten 4 comentarios');
				return;
			}

			infoModal.comentarios.push({no: totalComentarios + 1, texto: infoModal.comentarioActual.texto});
		}

		infoModal.comentarioActual = null;
	};

	$scope.ocultarModalComentarios = function() {
		var comentarios = $scope.infoModalComentarios.comentarios;
		var comentariosCuentaActual = $scope.estado.cuentaActual.comentarios;

		comentariosCuentaActual.length = 0;

		if(comentarios.length > 0)
			for (var i = 0; i < comentarios.length; i++)
				comentariosCuentaActual.push(comentarios[i].texto);

		comentarios.length = 0;
		$scope.modalComentarios.hide();
	};

	$scope.ocultarModal = function() {
		$scope.modalAnomalias.hide();
		$scope.modalPaginas.hide();
	};

	$scope.tomarFoto = function() {

		$ionicActionSheet.show({
		    buttons: [
		    	{ text: 'Cámara fotográfica' },
		    	{ text: 'Galería del teléfono' }
		    ],
		    buttonClicked: function(index) {
		    	switch (index) {
		    		case 0:
		    			ServicioFotografias.tomarFoto()
		    			.then(function(uri) {
		    				$scope.estado.cuentaActual.uri = uri;
		    			});
	    			break;
	    			case 1:
	    				ServicioFotografias.tomarFoto(true)
	    				.then(function(uri) {
	    					$scope.estado.cuentaActual.uri = uri;
	    				});
	    			break;
		    	}
		        return true;
		    }
		});
	};

	$scope.modificarLectura = function() {
		var cuentaActual = $scope.estado.cuentaActual;

		if(cuentaActual.estatus === 'capturado')
			cuentaActual.estatus = 'modificado';
		else{

			if(cuentaActual.lectura <= cuentaActual.lecturaAnterior)
			{
				ServicioAlertas.mostrarAlerta('La lectura tiene que ser mayor a la anterior');
				return;
			}

			ServicioLoading.mostrarLoading('Guardando');

			if(!cuentaActual.latitud && !cuentaActual.latitud)
				ServicioGPS.obtenerUbicacion()
				.then(function(posicion) {
					cuentaActual.latitud = posicion.latitud;
					cuentaActual.longitud = posicion.longitud;

					return ServicioSqlite.actualizarCuenta(cuentaActual);
				})
				.then(function() {
					cuentaActual.estatus = 'capturado';
					guardarProgreso();
				})
				.catch(function(error) {
					ServicioAlertas.mostrarAlerta('Ocurrio un problema al guardar la lectura', 'Error');
				})
				.finally(function() {
					ServicioLoading.ocultarLoading();
				});

			else
				ServicioSqlite.actualizarCuenta(cuentaActual)
				.then(function() {
					cuentaActual.estatus = 'capturado';
				})
				.catch(function(error) {
					ServicioAlertas.mostrarAlerta('Ocurrio un problema al guardar la lectura', 'Error');
				})
				.finally(function() {
					ServicioLoading.ocultarLoading();
				});
		}

		console.log($scope.estado.cuentaActual);
	};

	function buscar() {
		var myPopup = $ionicPopup.show({
			template: '<input type="number" ng-model="estado.filtro" autofocus>',
			title: 'Ingrese el filtro',
			subTitle: 'RPU, No Cuenta o No Medidor',
			scope: $scope,
			buttons: [
			    { text: 'Cancelar' }, {
			        text: '<b>Buscar</b>',
			        type: 'button-positive',
			        onTap: function(e) {
			            if (!$scope.estado.filtro)
			                e.preventDefault();
			            else 
			                return $scope.estado.filtro;
			        }
			    }
			]
		});

		myPopup.then(function(filtro) {
			if(filtro){
				ServicioLoading.mostrarLoading('Buscando');

				ServicioSqlite.buscarCuenta(filtro)
				.then(posicionarEnSlideBox)
				.then(function(predios) {
					$scope.estado.predios = predios;
					$scope.estado.cuentaActual = predios[index];
					$scope.slider.updateLoop();
					$scope.estado.filtro = null;
					deslisando = index === 0 ? false : true;
					$scope.slider.slideTo(index);

				})
				.catch(function(error) {
					ServicioAlertas.mostrarAlerta(error, 'Busqueda');
				})
				.finally(ServicioLoading.ocultarLoading);
			}
		});
	}

	function guardarProgreso() {
		var cuenta = $scope.estado.cuentaActual;
		almacenarDatos('progreso', cuenta.id);
	}

	function posicionarEnSlideBox(id) {
		var paginaActual = parseInt(id / FILAS_PAGINACION, 10);

		var tmp = id;

		while (tmp > 25)
			tmp -= 25;

		index = tmp - 1;

		if(id % FILAS_PAGINACION !== 0)
			paginaActual++;

		$scope.estado.paginaActual = paginaActual;
		return ServicioSqlite.paginar($scope.estado.paginaActual);
	}
});